package com.adit.photos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.drawable.Icon;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.adit.photos.Adapter.AdapterListPhoto;
import com.adit.photos.Helper.API.APIClient;
import com.adit.photos.Helper.API.APIInterface;
import com.adit.photos.Helper.Http;
import com.adit.photos.Model.ModelPhoto;
import com.google.android.material.textfield.TextInputEditText;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    @BindView(R.id.CariPhoto)
    EditText CariPhoto;
    @BindView(R.id.Rvlist_photos)
    RecyclerView RvlistPhotos;
    private Intent intent;
    private RecyclerView.Adapter adapter;
    private GridLayoutManager gridLayoutManager;
    private List<Icon> iconList;
    private int spanCount = 1;
    private APIInterface apiInterface;
    private APIClient ApiClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        ListPhotos();
    }
    private void ListPhotos() {
        RvlistPhotos.setLayoutManager(new GridLayoutManager(MainActivity.this, 1, LinearLayoutManager.VERTICAL, false));
        RvlistPhotos.setHasFixedSize(true);
        RvlistPhotos.setNestedScrollingEnabled(false);
        RvlistPhotos.setLayoutManager(new LinearLayoutManager(this));
        apiInterface = APIClient.getClient(Http.getUrl()).create(APIInterface.class);
        Call<List<ModelPhoto>>call = apiInterface.doGetAPIPhoto();
        call.enqueue(new Callback<List<ModelPhoto>>() {
            @Override
            public void onResponse(Call<List<ModelPhoto>> call, Response<List<ModelPhoto>> response) {
                Log.e(TAG, "onResponse: " + response.body());
                    List<ModelPhoto> listPhotos = response.body();
                    AdapterListPhoto adapterListPhoto = new AdapterListPhoto(MainActivity.this,  0, listPhotos);
                    adapterListPhoto.notifyDataSetChanged();
                    RvlistPhotos.setAdapter(adapterListPhoto);
            }

            @Override
            public void onFailure(Call<List<ModelPhoto>> call, Throwable t) {
                Toast.makeText(MainActivity.this, "Gagal Terhubung dengan server.", Toast.LENGTH_SHORT).show();
                Log.e(TAG, "onFailure: " + t.getMessage());
            }
        });

    }
}