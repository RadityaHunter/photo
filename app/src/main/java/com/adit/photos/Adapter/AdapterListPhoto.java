package com.adit.photos.Adapter;

import androidx.recyclerview.widget.RecyclerView;

import com.adit.photos.Helper.Http;
import com.adit.photos.Model.ModelPhoto;
import com.adit.photos.R;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.squareup.picasso.Picasso;

public class AdapterListPhoto extends RecyclerView.Adapter<AdapterListPhoto.myViewHolder> {
    private static final String TAG = AdapterListPhoto.class.getSimpleName();
    private Context context;
    private List<ModelPhoto> list;
    //    private AdapterAPICustomerCallback mAdapterCallback;
    private int result = -1;


    public AdapterListPhoto(Context context, int result, List<ModelPhoto> list) {
        this.context = context;
        this.list = list;
        this.result = result;

    }

    @NonNull
    @Override
    public AdapterListPhoto.myViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_list_photos, viewGroup, false);
        return new myViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull myViewHolder myViewHolder, int position) {
        List<ModelPhoto> item = list;
        myViewHolder.Menutv_title.setText(item.get(position).getTitle());
        Picasso.with(context).load(item.get(position).getThumbnailUrl()).resize(240, 240).into(myViewHolder.Menupicturesalepath);
//        Uri url_images = Uri.parse(Http.getUrl_Image() + item.get(position).getUrl().replace("~/", ""));
//        Log.d("url_images", url_images.toString());
//        Glide.with(context)
//                .load(url_images)
//                .apply(new RequestOptions().override(175, 175))
//                .into(myViewHolder.Menupicturesalepath);
    }

    @Override
    public int getItemCount() {
        if (list.size() == 0) {
            return 0;
        } else {
            if (result >= 1) {
                return Math.min(list.size(), result);
            } else {
                return list.size();
            }
        }
    }

    public void clear() {
        int size = this.list.size();
        this.list.clear();
        notifyItemRangeRemoved(0, size);
    }

    public interface AdapterAPICustomerCallback {
        void onRowAdapterCari2Clicked(ModelPhoto modelPhoto);

    }

    public class myViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.picturesalepath)
        ImageView Menupicturesalepath;
        @BindView(R.id.tv_title)
        TextView Menutv_title;

        public myViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
