package com.adit.photos.Helper.API;


import com.adit.photos.Model.ModelPhoto;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;


public interface APIInterface {
    @GET("/photos")
    Call<List<ModelPhoto>> doGetAPIPhoto();
}
